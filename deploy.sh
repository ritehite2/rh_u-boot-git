#!/bin/bash

# Deploy script tslib

set -e

cd $(dirname $0)

DEFAULT_DEPLOY=../deploy/boot
NUM_ARGS=$#

if [ "$NUM_ARGS" -eq "1" ]
then
	DEPLOY_PATH="$1"
else
	DEPLOY_PATH="$DEFAULT_DEPLOY"
	echo "No path passed, using default deploy path: "$DEPLOY_PATH""
	echo ""
fi

if [ ! -e "$DEPLOY_PATH" ]
then
	echo "Required deploy path does not exist, exiting"
	exit 1
fi

if [ ! -e MLO ] || [ ! -e u-boot.img ]
then
	echo "Missing deploy files, a build is required before deploying, exiting"
	exit 1
fi

echo "#### Deploying MLO ####"
echo ""
cp MLO "$DEPLOY_PATH"
echo "#### Deploying u-boot.img ####"
echo ""
cp u-boot.img "$DEPLOY_PATH"
echo "#### Deploying uEnv.txt ####"
echo ""

#Easiest way to capture this for now
#disables blanking of the console
#currently overriding silent option for now
touch "$DEPLOY_PATH"/uEnv.txt

echo "optargs=consoleblank=0" > "$DEPLOY_PATH"/uEnv.txt

#supposed to be new line at end
echo "" >> "$DEPLOY_PATH"/uEnv.txt

echo "U-boot deploy - COMPLETE"
echo ""

exit 0
