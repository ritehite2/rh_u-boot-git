#!/bin/bash

set -e 

cd $(dirname $0)

#DEFAULT_TOOLCHAIN_PATH=~/ti-processor-sdk-linux-am335x-evm-02.00.01.07/linux-devkit/sysroots/x86_64-arago-linux/usr/bin
SPECIFIED_TOOLCHAIN_PATH='' 
ARCHITECTURE=arm
CROSS_COMPILER=arm-linux-gnueabihf-
NUM_ARGS="$#"
    
echo ""

if [ $NUM_ARGS -eq 0 ]
then
    echo "No argments passed, building with default toolchain path: $DEFAULT_TOOLCHAIN_PATH"
    TOOL_CHAIN_PATH="$DEFAULT_TOOLCHAIN_PATH"
else
    TOOL_CHAIN_PATH="$1"
fi

#strong likelihood that we are in the right place if this file exists
if [ ! -e "$TOOL_CHAIN_PATH"/arm-linux-gnueabihf-gcc ]
then
    echo "Toolchain path $TOOL_CHAIN_PATH does not appear to contain the correct tools, exiting"
    echo ""
    exit 1
fi

export PATH="$TOOL_CHAIN_PATH":$PATH
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabihf-

echo "#### Configuring Build ####"
echo " "

make mrproper
make am335x_evm_defconfig

echo "#### Building U-Boot ####"
echo " "
make -j8

echo "#### Build Complete ####"
echo " "
